# Upute
Upute opisuju na koji način će se kolabroativno preko GitLab-a dodavati Terminologiju. Poglavlje u kojem se nalaze Upute, pa sve do prvog idućeg poglavlja ne smije se brisati. Format zapisivanja terminologije je sljedeći:  

**Pojam**; engl. verzija pojma  
(Alternative na hrvatskom; alternative na engleskom).

Iznad pojma i ispod pojma će bit prazni redak. Cilj je poredati pojmove redom kojim su se pojavljivali u predavanjima pa se mogu dodati potpoglavlja koja odgovoraju rednom broju predavanja.

Primjer:  
**Dionici**; engl. stakeholders  
(učesnici)

Dodatno, poglavlje **Izdvojeni sadržaj** je predviđeno za izdvajanje bitnih stvari iz predavanja te se također mogu dodati potpoglavlja koja odgovaraju rednom broju predavanja. Od vrha prema dnu se nabrajaju bitni pojmovi, bitniji pojmovi nalaze se bliže vrhu poglavlja. 

# Terminologija
## Predavanje 01

# Izdvojeni sadržaj
